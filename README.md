## Steps to reproduce

1. `git clone yarn-workspaces-problem-01`
1. `cd yarn-workspaces-problem-01`
1. `yarn` -> It uses workspaces, so should be enough
1. cd my-application
1. ember server

## What happens

```
 ~/yarn-workspaces-problem-01/my-application 
 ember server
Could not start watchman
Visit https://ember-cli.com/user-guide/#watchman for more info.
You have installed "ember-power-calendar" but you don't have any of the required meta-addons to make it work. Please, explicitly install 'ember-power-calendar-moment' or 'ember-power-calendar-luxon' in your app


Stack Trace and Error Report: /tmp/error.dump.f6fe9d5436c31a242af2eb0083c11211.log
```

## What should happen

Ember server should start

## Notes

 - I tried to add `ember-power-calendar-luxon` do `my-addon.dependencies`, did not help.

```
 ember --version
ember-cli: 3.1.2
node: 10.11.0
os: linux x64
 yarn --version
1.10.1
```

